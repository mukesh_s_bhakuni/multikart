package validators

import (
	"bytes"
	"fmt"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	"github.com/xeipuuv/gojsonschema"
)

func ValidateUserObject(requestBody string) (bool, error) {
	logger := loggerutils.GetLogger()
	schemaStr := `
	{
		"$schema": "http://json-schema.org/draft-04/schema#",
		"type": "object",
		"properties": {
			"_id" : {"type" : "string"},
			"userFirstName" : {"type": "string"},
			"userMiddleName" : {"type": "string"},
			"userLastName" : {"type" : "string"},
			"emailId" : {
						  "type" : "string",
						  "pattern" : "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
						},
			"userName" : {"type" : "string"},
			"password" :{"type" : "string"},
			"mobile" : {"type" : "array",
							"items" : {
								"type" : "string",
								"pattern" : "^[6-9][0-9]{9}$"
							}
						},
			"user_address" : {
				"type" : "array",
				"items" : {
					"type" : "object",
					"properties" : {
						"flat_no" : {"type" : "string"},
						"society" : {"type" : "string"},
						"street" : {"type" : "string"},
						"landmark" : {"type" : "string"},
						"city" : {"type" : "string"},
						"state" : {"type" : "string"},
						"pin_code" : {"type" : "string"},
						"country" : {"type" : "string"}
					},
					"required" : ["flat_no","society","street","landmark","city","state","pin_code","country"]
				}  
			}
		},
		"required" : ["userFirstName","userLastName","emailId","userName","password","user_address"]
	}`

	schema := gojsonschema.NewStringLoader(schemaStr)
	content := gojsonschema.NewStringLoader(requestBody)
	result, err := gojsonschema.Validate(schema, content)
	if err != nil {
		logger.Errorf("Invalid Json Schema Error: %v", err)
		return false, err
	}

	logger.Infof("Validation Result: %v", result.Errors())
	if !result.Valid() {
		var buffer bytes.Buffer
		for _, resulterr := range result.Errors() {
			logger.Debugf("- %s\n", resulterr)
			errString := fmt.Sprintf("Field : %s - %s", resulterr.Field(), resulterr.Description())
			buffer.Write([]byte(errString))
		}
		errorDesc := buffer.String()
		return false, customErrors.BadRequest(errorDesc)
	}
	return true, nil
}
