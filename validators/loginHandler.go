package validators

import (
	"bytes"
	"fmt"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	"github.com/xeipuuv/gojsonschema"
)

func ValidateLoginObject(requestBody string) (bool, error) {
	logger := loggerutils.GetLogger()
	schemaStr := `
	{
		"$schema": "http://json-schema.org/draft-04/schema#",
		"type": "object",
		"properties": {
			"username" : {"type" : "string"},
			"password" : {"type" : "string"}
		},
		"required" : ["username","password"]
	}`

	schema := gojsonschema.NewStringLoader(schemaStr)
	content := gojsonschema.NewStringLoader(requestBody)
	result, err := gojsonschema.Validate(schema, content)
	if err != nil {
		logger.Errorf("Invalid Json Schema Error: %v", err)
		return false, err
	}

	logger.Infof("Validation Result: %v", result.Errors())
	if !result.Valid() {
		var buffer bytes.Buffer
		for _, resulterr := range result.Errors() {
			logger.Debugf("- %s\n", resulterr)
			errString := fmt.Sprintf("Field : %s - %s", resulterr.Field(), resulterr.Description())
			buffer.Write([]byte(errString))
		}
		errorDesc := buffer.String()
		return false, customErrors.BadRequest(errorDesc)
	}
	return true, nil
}
