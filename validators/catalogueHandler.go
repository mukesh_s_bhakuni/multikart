package validators

import (
	"multiKart/constants"
	"multiKart/dataAccessObjects"
	"multiKart/loggerutils"
)

func ValidateWithDescisionTree(proposedCatalogue dataAccessObjects.ProposedCatalogue) bool {
	logger := loggerutils.GetLogger()
	if proposedCatalogue.CatalogueType == constants.HealthCare {
		logger.Info("1")
		if proposedCatalogue.Margin >= 50.0 {
			logger.Info("2")
			if proposedCatalogue.Ratings > 3 {
				logger.Info("3")
				if proposedCatalogue.UnitsSold > 500 && proposedCatalogue.ReturnedRatio < 0.5 {
					logger.Info("4")
					return true
				}
				return false
			}
			return false
		}
		return false
	}
	return false
}
