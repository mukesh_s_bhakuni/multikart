package validators

import (
	"bytes"
	"fmt"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	"github.com/xeipuuv/gojsonschema"
)

func ValidateCartObject(requestBody string) (bool, error) {
	logger := loggerutils.GetLogger()
	schemaStr := `
	{
		"$schema": "http://json-schema.org/draft-04/schema#",
		"type": "object",
		"properties": {
			"id" : {"type" : "string"},
			"products" : { 
				"type" : "object",
				"properties" : {
					"catalogue_id" : {"type" : "string"},
					"name" : {"type" : "string"},
					"alias" : {"type" : "string"},
					"retailer" : {"type" : "string"},
					"organization" : {"type" : "string"},
					"image_url" : {"type" : "string"},
					"price" : {"type" : "number"},
					"discount" : {"type" : "number"},
					"ratings" : {"type" : "number"},
					"in_stock" : {"type" : "boolean"}
				} 
			}
			"status" : {"type" : "string"},
			"total_price" : {"type" : "number"},
			"last_updated" : {"type" : "string"},
			"user_id" : {"type" : "string"},
		},
		"required" : ["id","status","total_price","last_updated","user_id","products"]
	}`

	schema := gojsonschema.NewStringLoader(schemaStr)
	content := gojsonschema.NewStringLoader(requestBody)
	result, err := gojsonschema.Validate(schema, content)
	if err != nil {
		logger.Errorf("Invalid Json Schema Error: %v", err)
		return false, err
	}

	logger.Infof("Validation Result: %v", result.Errors())
	if !result.Valid() {
		var buffer bytes.Buffer
		for _, resulterr := range result.Errors() {
			logger.Debugf("- %s\n", resulterr)
			errString := fmt.Sprintf("Field : %s - %s", resulterr.Field(), resulterr.Description())
			buffer.Write([]byte(errString))
		}
		errorDesc := buffer.String()
		return false, customErrors.BadRequest(errorDesc)
	}
	return true, nil
}
