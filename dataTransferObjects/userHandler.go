package dataTransferObjects

type ForgotPassword struct {
	EmailId string
	Mobile  string
}
