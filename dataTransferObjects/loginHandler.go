package dataTransferObjects

type LoginData struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}
