package dataAccessObjects

import (
	"multiKart/constants"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	uuid "github.com/satori/go.uuid"
	"gopkg.in/mgo.v2/bson"
)

type Address struct {
	FlatNo   string `json:"flat_no,omitempty" bson:"flat_no,omitempty"`
	Society  string `json:"society,omitempty" bson:"society,omitempty"`
	Street   string `json:"street,omitempty" bson:"street,omitempty"`
	Landmark string `json:"landmark,omitempty" bson:"landmark,omitempty"`
	City     string `json:"city,omitempty" bson:"city,omitempty"`
	State    string `json:"state,omitempty" bson:"state,omitempty"`
	PinCode  string `json:"pin_code,omitempty" bson:"pin_code,omitempty"`
	Country  string `json:"country,omitempty" bson:"country,omitempty"`
}

type UserDetails struct {
	Id             string    `json:"_id,omitempty" bson:"_id,omitempty"`
	UserFirstName  string    `json:"userFirstName,omitempty" bson:"userFirstName,omitempty"`
	UserMiddleName string    `json:"userMiddleName,omitempty" bson:"userMiddleName,omitempty"`
	UserLastName   string    `json:"userLastName,omitempty" bson:"userLastName,omitempty"`
	EmailId        string    `json:"emailId,omitempty" bson:"emailId,omitempty"`
	UserName       string    `json:"userName,omitempty" bson:"userName,omitempty"`
	Password       string    `json:"password,omitempty" bson:"password,omitempty"`
	Mobile         []string  `json:"mobile,omitempty" bson:"mobile,omitempty"`
	UserAddress    []Address `json:"user_address,omitempty" bson:"user_address,omitempty"`
}

func (u *UserDetails) CreateOrUpdateUser() (UserDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	if u.Id == "" {
		uuid := uuid.Must(uuid.NewV4())
		u.Id = uuid.String()
	}
	clctn := mgoSession.DB(constants.DBname).C(constants.UserCollection)
	_, err := clctn.UpsertId(u.Id, u)
	if err != nil {
		logger.Info("Unable To Create/Update UserCollection")
		return *u, customErrors.InternalError("Unable To Create/Update UserCollection")
	}
	return *u, nil
}

func GetUserDetailsById(Id string) (UserDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	clctn := mgoSession.DB(constants.DBname).C(constants.UserCollection)
	var userDetails UserDetails
	err := clctn.Find(bson.M{"_id": Id}).One(&userDetails)
	if err != nil {
		logger.Info("Unable To Find User With Given Id")
		return userDetails, customErrors.InternalError("Unable To Find User With Given Id")
	}
	return userDetails, nil
}

func GetUserDetailsByUserName(username string) (UserDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	clctn := mgoSession.DB(constants.DBname).C(constants.UserCollection)
	var userDetails UserDetails
	err := clctn.Find(bson.M{"userName": username}).One(&userDetails)
	if err != nil {
		logger.Info("Unable To Find User With Given UserName")
		return userDetails, customErrors.InternalError("Unable To Find User With Given UserName")
	}
	return userDetails, nil
}

func GetUserDetailsByEmailId(emailId string) (UserDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	clctn := mgoSession.DB(constants.DBname).C(constants.UserCollection)
	var userDetails UserDetails
	err := clctn.Find(bson.M{"emailId": emailId}).One(&userDetails)
	if err != nil {
		logger.Info("Unable To Find User With Given emailID")
		return userDetails, customErrors.InternalError("Unable To Find User With Given emailID")
	}
	return userDetails, nil
}

func GetUserDetailsFromTokenId(tokenId string) (*UserDetails, error) {
	logger := loggerutils.GetLogger()
	loginDetails, err := GetLoginSessionDetailsFromTokenId(tokenId)
	if err != nil {
		logger.Info(err)
		return nil, err
	}
	userDetails, err := GetUserDetailsByUserName(loginDetails.Username)
	if err != nil {
		logger.Info(err)
		return nil, err
	}
	return &userDetails, nil
}
