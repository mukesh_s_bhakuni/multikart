package dataAccessObjects

import (
	"multiKart/constants"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	uuid "github.com/satori/go.uuid"
	"gopkg.in/mgo.v2/bson"
)

type Cart struct {
	Id          string      `json:"id,omitempty" bson:"id,omitempty"`
	Products    []Catalogue `json:"products,omitempty" bson:"products,omitempty"`
	Status      string      `json:"status,omitempty" bson:"status,omitempty" `
	TotalPrice  float64     `json:"total_price,omitempty bson:"total_price,omitempty"`
	LastUpdated string      `json:"last_updated,omitempty" bson:"last_updated,omitempty"`
	UserId      string      `json:"user_id,omitempty" bson:"user_id,omitempty"`
}

func (c *Cart) CreateOrUpdateCart() (Cart, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	if c.Id == "" {
		uuid := uuid.Must(uuid.NewV4())
		c.Id = uuid.String()
	}
	clctn := mgoSession.DB(constants.DBname).C(constants.CartCollection)
	_, err := clctn.UpsertId(c.Id, c)
	if err != nil {
		logger.Info("Unable To Create/Update Cart", err)
		return *c, customErrors.InternalError("Unable To Create/Update Cart " + err.Error())
	}
	return *c, nil
}

func GetCartDetailsFromCartId(CartId string) (Cart, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()

	clctn := mgoSession.DB(constants.DBname).C(constants.CartCollection)
	cartDetails := Cart{}
	err := clctn.Find(bson.M{"id": CartId}).One(&cartDetails)
	if err != nil {
		logger.Info("Unable To Get CartDetails By Id")
		return cartDetails, customErrors.InternalError("Unable To Get Cart By Id")
	}
	return cartDetails, nil
}

func GetCartDetailsFromUserId(UserId string) (Cart, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()

	clctn := mgoSession.DB(constants.DBname).C(constants.CartCollection)
	cartDetails := Cart{}
	err := clctn.Find(bson.M{"user_id": UserId}).One(&cartDetails)
	if err != nil {
		logger.Info("Unable To Get CartDetails By UserId")
		return cartDetails, customErrors.InternalError("Unable To Get Cart By UserId")
	}
	return cartDetails, nil
}
