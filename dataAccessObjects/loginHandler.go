package dataAccessObjects

import (
	"multiKart/constants"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	uuid "github.com/satori/go.uuid"
	"gopkg.in/mgo.v2/bson"
)

type LoginSessionDetails struct {
	LoginStartTime uint64 `json:"login_start_time,omitempty" bson:"login_start_time,omitempty"`
	LoginEndTime   uint64 `json:"login_end_time,omitempty" bson:"login_end_time,omitempty"`
	TokenId        string `json:"_id,omitempty" bson:"_id,omitempty"`
	Username       string `json:"username,omitempty" bson:"username,omitempty"`
	Status         string `json:"status,omitempty" bson:"status,omitempty"`
}

func (s *LoginSessionDetails) CreateOrUpdateSession() (LoginSessionDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	if s.TokenId == "" {
		uuid := uuid.Must(uuid.NewV4())
		s.TokenId = uuid.String()
	}
	clctn := mgoSession.DB(constants.DBname).C(constants.UserSessionCollection)
	_, err := clctn.UpsertId(s.TokenId, s)
	if err != nil {
		logger.Info("Unable To Create/Update UserSessionCollection")
		return *s, customErrors.InternalError("Unable To Create/Update UserSessionCollection")
	}
	return *s, nil
}

func GetLoginSessionDetailsFromTokenId(tokenId string) (LoginSessionDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	var loginSessionDetails LoginSessionDetails
	clctn := mgoSession.DB(constants.DBname).C(constants.UserSessionCollection)
	err := clctn.Find(bson.M{"_id": tokenId}).One(&loginSessionDetails)
	if err != nil {
		logger.Info("Unable To Get UserSessionCollection")
		return loginSessionDetails, customErrors.InternalError("Unable To Get UserSessionCollection")
	}
	return loginSessionDetails, nil
}

func GetActiveLoginSessionDetailsFromUsername(username string) (LoginSessionDetails, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	var loginSessionDetails LoginSessionDetails
	clctn := mgoSession.DB(constants.DBname).C(constants.UserSessionCollection)
	err := clctn.Find(bson.M{"username": username, "status": constants.Active}).One(&loginSessionDetails)
	if err != nil {
		logger.Info("Unable To Get UserSessionCollection")
		return loginSessionDetails, customErrors.InternalError("Unable To Get UserSessionCollection")
	}
	return loginSessionDetails, nil
}
