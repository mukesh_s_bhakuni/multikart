package dataAccessObjects

import (
	"multiKart/constants"
	"multiKart/customErrors"
	"multiKart/loggerutils"

	uuid "github.com/satori/go.uuid"
	"gopkg.in/mgo.v2/bson"
)

type Catalogue struct {
	CatalogueID   string  `json:"catalogue_id,omitempty" bson:"catalogue_id,omitempty"`
	Id            string  `json:"_id,omitempty" bson:"_id,omitempty"`
	Name          string  `json:"name,omitempty" bson:"name,omitempty"`
	Alias         string  `json:"alias,omitempty" bson:"alias,omitempty"`
	Price         float64 `json:"price,omitempty" bson:"price,omitempty"`
	Discount      float64 `json:"discount,omitempty" bson:"discount,omitempty"`
	Retailer      string  `json:"retailer,omitempty" bson:"retailer,omitempty"`
	Organization  string  `json:"organization,omitempty" bson:"organization,omitempty"`
	ImageUrl      string  `json:"image_url,omitempty" bson:"image_url,omitempty"`
	Ratings       int64   `json:"ratings,omitempty" bson:"ratings,omitempty"`
	InStock       bool    `json:"in_stock,omitempty" bson:"in_stock,omitempty"`
	Info          string  `json:"info,omitempty" bson:"info,omitempty"`
	CatalogueType string  `json:"catalogue_type,omitempty" bson:"catalogue_type,omitempty"`
}

type ProposedCatalogue struct {
	OrganizationName string
	CatalogueName    string
	CatalogueID      string
	CatalogueType    string
	Price            float64
	Discount         float64
	Retailer         string
	Margin           float64
	Ratings          int64
	UnitsSold        int64
	ReturnedRatio    float64
	ImageUrl         string
	Info             string
}

func (c *Catalogue) CreateOrUpdateCatalogue() (Catalogue, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	defer mgoSession.Close()
	if c.Id == "" {
		uuid := uuid.Must(uuid.NewV4())
		c.Id = uuid.String()
	}
	clctn := mgoSession.DB(constants.DBname).C(constants.CatalogueCollection)
	_, err := clctn.UpsertId(c.Id, c)
	if err != nil {
		logger.Info("Unable To Create/Update Catalogue", err)
		return *c, customErrors.InternalError("Unable To Create/Update Catalogue " + err.Error())
	}
	return *c, nil
}

func GetCatalogueById(Id string) (Catalogue, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()

	clctn := mgoSession.DB(constants.DBname).C(constants.CatalogueCollection)
	catalogueDetails := Catalogue{}
	err := clctn.Find(bson.M{"_id": Id}).One(&catalogueDetails)
	if err != nil {
		logger.Info("Unable To Get Catalogue By Id")
		return catalogueDetails, customErrors.InternalError("Unable To Get Catalogue By Id")
	}
	return catalogueDetails, nil
}

func GetAllCatalogues() (*[]Catalogue, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()

	clctn := mgoSession.DB(constants.DBname).C(constants.CatalogueCollection)
	catalogues := []Catalogue{}
	err := clctn.Find(nil).All(&catalogues)
	if err != nil {
		logger.Info("Unable To Get Catalogues")
		return &catalogues, customErrors.InternalError("Unable To Get Catalogues")
	}
	return &catalogues, nil
}

func GetCataloguesByNameString(nameString string) (*[]Catalogue, error) {
	logger := loggerutils.GetLogger()
	mgoSession := MongoSession.Clone()
	clctn := mgoSession.DB(constants.DBname).C(constants.CatalogueCollection)
	catalogues := []Catalogue{}
	prefix := "^" + nameString
	suffix := nameString + "$"
	middle := "^.*" + nameString + ".*$"
	subQuery := []bson.M{bson.M{"name": bson.M{"$regex": bson.RegEx{Pattern: prefix, Options: "i"}}}}
	subQuery = append(subQuery, bson.M{"name": bson.M{"$regex": bson.RegEx{Pattern: suffix, Options: "i"}}})
	subQuery = append(subQuery, bson.M{"name": bson.M{"$regex": bson.RegEx{Pattern: middle, Options: "i"}}})
	err := clctn.Find(bson.M{"$or": subQuery}).All(&catalogues)
	if err != nil {
		logger.Info("Unable To Get Catalogues")
		return &catalogues, customErrors.InternalError("Unable To Get Catalogues")
	}
	return &catalogues, nil
}

func GetCataloguesByParams(nameString string, priceValue []int, discountValue []float64, category string) (*[]Catalogue, error) {
	logger := loggerutils.GetLogger()
	logger.Info(priceValue, discountValue, category)
	mgoSession := MongoSession.Clone()
	clctn := mgoSession.DB(constants.DBname).C(constants.CatalogueCollection)
	catalogues := []Catalogue{}
	prefix := "^" + nameString
	suffix := nameString + "$"
	middle := "^.*" + nameString + ".*$"

	subQuery := []bson.M{bson.M{"name": bson.M{"$regex": bson.RegEx{Pattern: prefix, Options: "i"}}}}
	subQuery = append(subQuery, bson.M{"name": bson.M{"$regex": bson.RegEx{Pattern: suffix, Options: "i"}}})
	subQuery = append(subQuery, bson.M{"name": bson.M{"$regex": bson.RegEx{Pattern: middle, Options: "i"}}})

	priceQuery := []bson.M{}
	for _, priceValue := range priceValue {
		priceRange := constants.ValueToPriceMatch(priceValue)
		priceQuery = append(priceQuery, bson.M{
			"in_stock": true,
			"price": bson.M{
				"$gte": priceRange[0],
				"$lte": priceRange[1],
			}})
	}

	discountQuery := []bson.M{}
	for _, discountValue := range discountValue {
		discountQuery = append(discountQuery, bson.M{
			"discount": bson.M{
				"$gte": discountValue,
			}})
	}

	orPriceQuery := []bson.M{}
	if len(priceQuery) > 0 {
		orPriceQuery = append(orPriceQuery, bson.M{"$or": priceQuery})
	}
	orPriceQuery = append(orPriceQuery, bson.M{"$or": subQuery})
	if len(discountQuery) > 0 {
		orPriceQuery = append(orPriceQuery, bson.M{"$or": discountQuery})
	}
	err := clctn.Find(bson.M{"$and": orPriceQuery, "catalogue_type": category}).All(&catalogues)
	if err != nil {
		logger.Info("Unable To Get Catalogues")
		return &catalogues, customErrors.InternalError("Unable To Get Catalogues")
	}
	return &catalogues, nil
}
