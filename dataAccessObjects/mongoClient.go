package dataAccessObjects

import (
	// envconfig "bitbucket.org/agrostar/fcopstracker/config"

	"multiKart/loggerutils"

	mgo "gopkg.in/mgo.v2"
)

//MongoSession stores mongo session
var MongoSession *mgo.Session

//RegisterMongoSession creates a new mongo session
// func RegisterMongoSession(mongoURI string) {
// 	var err error
// 	MongoSession, err = mgo.Dial(mongoURI)
// 	if err != nil {
// 		logger.Errorf("Error in Connecting Mongo")
// 		panic(err)
// 	}
// 	username := envconfig.SessionConfig.MongoDBUsername
// 	password := envconfig.SessionConfig.MongoDBPassword
// 	err2 := MongoSession.DB(constants.DbName).Login(username, password)
// 	if err2 != nil {
// 		logger.Errorf("Not able to login to Mongo with username and password")
// 		panic(err2)
// 	}
// }

func init() {
	var err error
	logger := loggerutils.GetLogger()
	logger.Info("Connecting To Localhost")
	MongoSession, err = mgo.Dial("localhost:27017")
	if err != nil {
		//logger.Errorf("Error in Connecting Mongo")
		panic(err)
	}
}
