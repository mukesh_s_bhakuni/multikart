package utils

import (
	"mime/multipart"
	"multiKart/customErrors"
	"multiKart/loggerutils"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"unicode/utf8"
)

func IsExists(slice interface{}, item interface{}) bool {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		return false
	}
	for i := 0; i < s.Len(); i++ {
		if s.Index(i).Interface() == item {
			return true
		}
	}
	return false
}

func IsEqualList(listA []string, listB []string) bool {
	if len(listA) != len(listB) {
		return false
	}
	for i, data := range listA {
		if data != listB[i] {
			return false
		}
	}
	return true
}

func RemoveUnicodeCharacterFromString(s string) string {
	if !utf8.ValidString(s) {
		v := make([]rune, 0, len(s))
		for i, r := range s {
			if r == utf8.RuneError {
				_, size := utf8.DecodeRuneInString(s[i:])
				if size == 1 {
					continue
				}
			}
			v = append(v, r)
		}
		s = string(v)
	}
	return s
}

func RemoveUnicodeFromStringList(stringList []string) []string {
	for i, v := range stringList {
		stringList[i] = RemoveUnicodeCharacterFromString(v)
	}
	return stringList
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func WriteTofile(filename string, text string) bool {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return false
	}
	defer f.Close()
	if _, err = f.WriteString(text); err != nil {
		return false
	}
	return true
}

func CompareName(name1 string, name2 string) bool {
	name1 = strings.ToLower(name1)
	name2 = strings.ToLower(name2)
	if removeMultipleSpace(name1) == removeMultipleSpace(name2) {
		return true
	}
	return false
}

func removeMultipleSpace(input string) string {
	side := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	middle := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	trimedString := middle.ReplaceAllString(input, " ")
	trimedString = side.ReplaceAllString(trimedString, "")
	return trimedString
}

func ParseAndGetFileHeader(r *http.Request) (*multipart.FileHeader, error) {
	logger := loggerutils.GetLogger()

	err := r.ParseMultipartForm(0)
	if err != nil {
		logger.Error("Error In ParsingMultipartForm Data ", err)
		return nil, customErrors.PreConditionFailedError("Error In ParsingMultipartForm Data ")
	}
	file, header, err := r.FormFile("file")
	if err != nil {
		logger.Error("Error In File Header")
		return nil, err
	}

	temp := make([]byte, 512)
	if _, err := file.Read(temp); err != nil {
		return nil, err
	}
	logger.Info(" file type  ", http.DetectContentType(temp))

	return header, nil
}

func ArrayStringToFloat(array []string) []float64 {
	logger := loggerutils.GetLogger()
	floatArray := []float64{}
	for _, elm := range array {
		num, err := strconv.ParseFloat(elm, 64)
		if err != nil {
			logger.Info(err)
			return []float64{}
		}
		floatArray = append(floatArray, num)
	}
	return floatArray
}

func ArrayStringToInt(array []string) []int {
	logger := loggerutils.GetLogger()
	intArray := []int{}
	for _, elm := range array {
		num, err := strconv.Atoi(elm)
		if err != nil {
			logger.Info(err)
			return []int{}
		}
		intArray = append(intArray, num)
	}
	return intArray
}
