package utils

import "time"

func GetDateByUtcTime(timeUint64 uint64) string {
	return (time.Unix(int64(int64(timeUint64)/1000), 0).Format("02-01-2006"))
}
