package utils

import "time"

//GetUTCTime returns utc ms for current time
func GetUTCTime() uint64 {
	return uint64(time.Now().Unix() * 1000)
}
