package utils

import (
	"net/http"
	"time"
)

//GetClient Gives http client for making requests
func GetClient() *http.Client {
	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr, Timeout: 2 * time.Minute}
	return client
}
