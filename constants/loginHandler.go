package constants

var Active string = "LoggedIn"
var InActive string = "LoggedOut"
var ChangeTypeLogin string = "login"
var ChangeTypeLogout string = "logout"
