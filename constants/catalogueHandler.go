package constants

const HealthCare string = "healthcare"

type M map[string]interface{}

func PriceRange() []M {
	var priceRange []M
	m1 := M{"priceValue": 1, "priceString": "Rs100 to Rs500", "isChecked": false}
	m2 := M{"priceValue": 2, "priceString": "Rs500 to Rs1000", "isChecked": false}
	m3 := M{"priceValue": 3, "priceString": "Rs1000 to Rs2000", "isChecked": false}
	m4 := M{"priceValue": 4, "priceString": "Rs2000 to Rs4000", "isChecked": false}
	priceRange = append(priceRange, m1, m2, m3, m4)
	return priceRange
}

func DiscountRange() []M {
	var discountRange []M
	m1 := M{"discountValue": 10, "discountString": "10% and above", "isChecked": false}
	m2 := M{"discountValue": 20, "discountString": "20% and above", "isChecked": false}
	m3 := M{"discountValue": 30, "discountString": "30% and above", "isChecked": false}
	m4 := M{"discountValue": 40, "discountString": "40% and above", "isChecked": false}
	m5 := M{"discountValue": 50, "discountString": "50%", "isChecked": false}
	discountRange = append(discountRange, m1, m2, m3, m4, m5)
	return discountRange
}

func Options() []string {
	options := []string{"All", "Men", "Women", "Kids", "Electronics", "healthcare"}
	return options
}

func ValueToPriceMatch(value int) []float64 {
	valueToPrice := map[int][]float64{
		1: []float64{100, 500},
		2: []float64{500, 1000},
		3: []float64{1000, 2000},
		4: []float64{2000, 4000},
	}
	return valueToPrice[value]
}
