package constants

const DBname string = "multiKart"

const UserCollection string = "users"

const UserSessionCollection string = "userSession"

const CatalogueCollection string = "catalogue"
