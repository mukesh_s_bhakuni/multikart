package serviceHandlers

import (
	"encoding/json"
	"io/ioutil"
	"multiKart/constants"
	"multiKart/customErrors"
	"multiKart/dataAccessObjects"
	"multiKart/dataTransferObjects"
	"multiKart/loggerutils"
	"multiKart/mthdroutr"
	"multiKart/resputl"
	"multiKart/utils"
	"multiKart/validators"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

type LoginHandler struct {
	BaseHandler
}

func (u *LoginHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	response := mthdroutr.RouteApiCall(u, r)
	response.RenderResponse(w)
}

func (u *LoginHandler) Get(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Get Request For LoginService")
	params := r.URL.Query()
	tokenId := params["tokenId"]
	logger.Info("tokenId ", tokenId)
	loginDetails, err := dataAccessObjects.GetLoginSessionDetailsFromTokenId(tokenId[0])
	if err != nil {
		logger.Info(err.Error())
		return resputl.ReponseCustomError(err)
	}
	if loginDetails.Status == constants.InActive {
		logger.Info("User Session Expired")
		return resputl.Simple404Response("User Session Expired")
	}
	return resputl.Response200OK(loginDetails)
}

func (u *LoginHandler) Put(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Put Request For LoginService")
	params := r.URL.Query()
	changeType := params["changeType"]
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Info("Request Error")
		return resputl.SimpleBadRequest("Request Error")
	}
	if changeType[0] == constants.ChangeTypeLogin {
		success, err := validators.ValidateLoginObject(string(body))
		if !success {
			logger.Info(err)
			return resputl.SimpleBadRequest(err.Error())
		}
		var loginDetail dataTransferObjects.LoginData
		err = json.Unmarshal(body, &loginDetail)
		if err != nil {
			logger.Info("Unable To Parse Json Object")
			return resputl.SimpleBadRequest("Unable To Parse Json Object")
		}
		userDetails, err := dataAccessObjects.GetUserDetailsByUserName(loginDetail.Username)
		if err != nil {
			logger.Info(err)
			return resputl.ReponseCustomError(err)
		}
		if userDetails.Password != loginDetail.Password {
			logger.Info("Incorrect Password")
			return resputl.ReponseCustomError(customErrors.InternalError("Incorrect Password"))
		}
		_, err = dataAccessObjects.GetActiveLoginSessionDetailsFromUsername(userDetails.UserName)
		if err == nil {
			logger.Info("user already logged in with the username on anathor machine")
			return resputl.ReponseCustomError(customErrors.PreConditionFailedError("user already logged in with the username on anathor machine"))
		}
		loginDetails := dataAccessObjects.LoginSessionDetails{
			LoginStartTime: utils.GetUTCTime(),
			Username:       userDetails.UserName,
			Status:         constants.Active,
			TokenId:        uuid.Must(uuid.NewV4()).String(),
		}
		loginDetails, err = loginDetails.CreateOrUpdateSession()
		if err != nil {
			logger.Info(err)
			return resputl.ReponseCustomError(err)
		}
		return resputl.Response200OK(loginDetails)
	} else if changeType[0] == constants.ChangeTypeLogout {
		tokenId := params["tokenId"]
		loginDetails, err := dataAccessObjects.GetLoginSessionDetailsFromTokenId(tokenId[0])
		if err != nil {
			return resputl.ReponseCustomError(err)
		}
		loginDetails.LoginEndTime = utils.GetUTCTime()
		loginDetails.Status = constants.InActive
		loginDetails.CreateOrUpdateSession()
		return resputl.Simple200OK("Logged Out Succesfully")
	} else {
		return resputl.SimpleBadRequest("Invalid Request")
	}
}
