package serviceHandlers

import (
	"encoding/json"
	"io/ioutil"
	"multiKart/dataAccessObjects"
	"multiKart/loggerutils"
	"multiKart/mthdroutr"
	"multiKart/resputl"
	"multiKart/validators"
	"net/http"
)

type CartHandler struct {
	BaseHandler
}

func (u *CartHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	response := mthdroutr.RouteApiCall(u, r)
	response.RenderResponse(w)
}

func (c *CartHandler) Get(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Get Request For CartHandler")
	params := r.URL.Query()
	cartId, ok1 := params["cartId"]
	if !ok1 {
		userId, ok2 := params["userId"]
		if !ok2 {
			logger.Info("url params not passed properly")
			return resputl.SimpleBadRequest("url params not passed properly")
		}
		cartDetails, err := dataAccessObjects.GetCartDetailsFromUserId(userId[0])
		if err != nil {
			logger.Info(err)
			resputl.ReponseCustomError(err)
		}
		return resputl.Response200OK(cartDetails)
	}
	cartDetails, err := dataAccessObjects.GetCartDetailsFromCartId(cartId[0])
	if err != nil {
		logger.Info(err.Error())
		return resputl.ReponseCustomError(err)
	}
	return resputl.Response200OK(cartDetails)
}

func (c *CartHandler) Put(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Put Request For CartHandler")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Info("Request Error")
		return resputl.SimpleBadRequest("Request Error")
	}
	success, err := validators.ValidateCartObject(string(body))
	if !success {
		logger.Info(err)
		return resputl.SimpleBadRequest(err.Error())
	}

	var cart dataAccessObjects.Cart
	err = json.Unmarshal(body, &cart)
	if err != nil {
		logger.Info("Invalid Json Object Passed")
		return resputl.PreconditionFailed("Invalid Json Object Passed")
	}

	cartDetails, err := cart.CreateOrUpdateCart()
	if err != nil {
		logger.Info(err)
		return resputl.ReponseCustomError(err)
	}
	return resputl.Response200OK(cartDetails)
}
