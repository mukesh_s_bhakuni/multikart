package serviceHandlers

import (
	"encoding/json"
	"io/ioutil"
	"multiKart/dataAccessObjects"
	"multiKart/loggerutils"
	"multiKart/mthdroutr"
	"multiKart/resputl"
	"multiKart/validators"
	"net/http"
)

type UserHandler struct {
	BaseHandler
}

func (u *UserHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	response := mthdroutr.RouteApiCall(u, r)
	response.RenderResponse(w)
}

func (u *UserHandler) Get(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Get Request For UserService")
	params := r.URL.Query()
	tokenId := params["tokenId"]
	userDetails, err := dataAccessObjects.GetUserDetailsFromTokenId(tokenId[0])
	if err != nil {
		logger.Info(err.Error())
		return resputl.ReponseCustomError(err)
	}
	return resputl.Response200OK(userDetails)
}

func (u *UserHandler) Post(r *http.Request) resputl.SrvcRes {
	logger = loggerutils.GetLogger()
	logger.Info("Post Request For UserService")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Info("Request Error")
		return resputl.SimpleBadRequest("Request Error")
	}
	logger.Info(string(body))
	success, err := validators.ValidateUserObject(string(body))
	if !success {
		logger.Info(err)
		return resputl.SimpleBadRequest(err.Error())
	}
	var userDetails dataAccessObjects.UserDetails
	err = json.Unmarshal(body, &userDetails)
	if err != nil {
		logger.Info("Unable To Parse Json Object", err)
		return resputl.SimpleBadRequest("Unable To Parse Json Object")
	}
	_, err = dataAccessObjects.GetUserDetailsByEmailId(userDetails.EmailId)
	if err == nil {
		logger.Info("User Already Exists With The Given EmailId")
		return resputl.SimpleBadRequest("User Already Exists With The Given EmailId")
	} else {
		_, err := dataAccessObjects.GetUserDetailsByUserName(userDetails.UserName)
		if err == nil {
			logger.Info("User Already Exists With The Given UserName")
			return resputl.SimpleBadRequest("User Already Exists With The Given UserName")
		}
	}
	userDetails, err = userDetails.CreateOrUpdateUser()
	if err != nil {
		logger.Info(err.Error())
		return resputl.SimpleBadRequest("Unable To Create/Update User")
	}
	return resputl.Response200OK(userDetails)
}

func (u *UserHandler) Put(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Put Request For UserService")
	params := r.URL.Query()
	Type := params["type"]

	if Type[0] == "updateBody" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logger.Info("Request Error")
			return resputl.SimpleBadRequest("Request Error")
		}
		success, err := validators.ValidateUserObject(string(body))
		if !success {
			logger.Info(err)
			return resputl.ReponseCustomError(err)
		}
		var userDetails dataAccessObjects.UserDetails
		err = json.Unmarshal(body, &userDetails)
		if err != nil {
			logger.Info("Unable To Parse Json Object")
			return resputl.SimpleBadRequest("Unable To Parse Json Object")
		}
		logger.Info(userDetails)
		userDetails, err = userDetails.CreateOrUpdateUser()
		if err != nil {
			logger.Info(err)
			return resputl.ReponseCustomError(err)
		}
		return resputl.Response200OK(userDetails)
	} else {
		return resputl.SimpleBadRequest("invalid request")
	}
}
