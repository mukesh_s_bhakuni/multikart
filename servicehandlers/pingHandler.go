package servicehandlers

import (
	"net/http"

	"multiKart/loggerutils"
	"multiKart/mthdroutr"
	"multiKart/resputl"
)

// PingHandler is a Basic ping utility for the service
type PingHandler struct {
	BaseHandler
}

func (p *PingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	response := mthdroutr.RouteApiCall(p, r)
	response.RenderResponse(w)
}

// Get function for PingHandler
func (p *PingHandler) Get(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Infof("Get Request For  Ping")
	return resputl.Response200OK("OK")
}
