package servicehandlers

import (
	"encoding/csv"
	"multiKart/constants"
	"multiKart/dataAccessObjects"
	"multiKart/loggerutils"
	"multiKart/mthdroutr"
	"multiKart/resputl"
	"multiKart/utils"
	"net/http"
	"strconv"
	"strings"

	uuid "github.com/satori/go.uuid"
)

type CatalogueHandler struct {
	BaseHandler
}

func (u *CatalogueHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	response := mthdroutr.RouteApiCall(u, r)
	response.RenderResponse(w)
}

func (c *CatalogueHandler) Get(r *http.Request) resputl.SrvcRes {
	logger := loggerutils.GetLogger()
	logger.Info("Get Request For CatalogueHandler")
	params := r.URL.Query()
	options := params["options"]
	if options[0] == "searchString" {
		discountValues, ok1 := params["discountValues"]
		priceValues, ok2 := params["priceValues"]
		category, ok3 := params["category"]
		var newDiscountValues []float64
		var newPriceValues []int
		var newCategory string
		if ok1 && len(discountValues) > 0 {
			newDiscountValues = utils.ArrayStringToFloat(strings.Split(discountValues[0], ","))
		}
		if ok2 && len(priceValues) > 0 {
			newPriceValues = utils.ArrayStringToInt(strings.Split(priceValues[0], ","))
		}
		if ok3 && len(category) > 0 {
			newCategory = category[0]
		}
		logger.Info("in here ", category, discountValues, priceValues)
		searchString := params["searchString"]

		// catalogues, err := dataAccessObjects.GetCataloguesByNameString(searchString[0])
		catalogues, err := dataAccessObjects.GetCataloguesByParams(searchString[0], newPriceValues, newDiscountValues, newCategory)
		if err != nil {
			logger.Info(err.Error())
			return resputl.ReponseCustomError(err)
		}
		logger.Info(catalogues)
		return resputl.Response200OK(catalogues)
	} else if options[0] == "constants" {
		var pageConstants []interface{}
		priceRange := constants.PriceRange()
		discountRange := constants.DiscountRange()
		options := constants.Options()
		pageConstants = append(pageConstants, priceRange, discountRange, options)
		return resputl.Response200OK(pageConstants)
	} else {
		return resputl.SimpleBadRequest("Invalid Options")
	}
}

func (c *CatalogueHandler) Post(r *http.Request) resputl.SrvcRes {
	fileHeader, err := utils.ParseAndGetFileHeader(r)
	if err != nil {
		logger.Info("Unable To Parse File Header")
		return resputl.SimpleBadRequest("Unable To Parse File Header")
	}
	csvFile, _ := fileHeader.Open()
	defer csvFile.Close()
	reader := csv.NewReader(csvFile)
	records, err := reader.ReadAll()
	if err != nil {
		logger.Info("Unable To Read The File")
		return resputl.SimpleBadRequest("Unable To Read The File")
	}
	for _, record := range records[1:] {
		price, _ := strconv.ParseFloat(record[4], 64)
		discount, _ := strconv.ParseFloat(record[5], 64)
		margin, _ := strconv.ParseFloat(record[7], 64)
		ratings, _ := strconv.ParseInt(record[8], 10, 64)
		unitSold, _ := strconv.ParseInt(record[9], 10, 64)
		returnedRatio, _ := strconv.ParseFloat(record[10], 64)

		proposedCatalogue := dataAccessObjects.ProposedCatalogue{
			CatalogueID:      record[0],
			CatalogueName:    record[1],
			OrganizationName: record[2],
			CatalogueType:    record[3],
			Price:            price,
			Discount:         discount,
			Retailer:         record[6],
			Margin:           margin,
			Ratings:          ratings,
			UnitsSold:        unitSold,
			ReturnedRatio:    returnedRatio,
			ImageUrl:         record[11],
			Info:             record[12],
		}
		//validate with descision tree
		logger.Info(proposedCatalogue)
		success := true //validators.ValidateWithDescisionTree(proposedCatalogue)
		if success {
			catalogue := dataAccessObjects.Catalogue{
				Id:            uuid.Must(uuid.NewV4()).String(),
				CatalogueID:   proposedCatalogue.CatalogueID,
				Name:          proposedCatalogue.CatalogueName,
				Price:         proposedCatalogue.Price,
				Discount:      proposedCatalogue.Discount,
				Retailer:      proposedCatalogue.Retailer,
				Organization:  proposedCatalogue.OrganizationName,
				ImageUrl:      proposedCatalogue.ImageUrl,
				Ratings:       proposedCatalogue.Ratings,
				InStock:       true,
				Info:          proposedCatalogue.Info,
				CatalogueType: proposedCatalogue.CatalogueType,
			}
			catalogue, err := catalogue.CreateOrUpdateCatalogue()
			if err != nil {
				logger.Info(err)
				return resputl.SimpleBadRequest("Unable To Create Catalogues")
			}
			logger.Info(catalogue.Name)
		}
	}
	return resputl.Simple200OK("All Catalogues Inserted Successfully")
}
