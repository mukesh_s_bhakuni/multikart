package main

import (
	"multiKart/loggerutils"
	"multiKart/servicehandlers"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func init() {
	http.DefaultClient.Timeout = time.Minute * 10
}

func main() {
	logger := loggerutils.GetLogger()
	pingHandler := &servicehandlers.PingHandler{}
	loginHandler := &servicehandlers.LoginHandler{}
	userHandler := &servicehandlers.UserHandler{}
	catalogueHandler := &servicehandlers.CatalogueHandler{}
	logger.Infof("Setting up resources.")
	h := mux.NewRouter()

	//handlers' url
	h.Handle("/multiKart/ping/", pingHandler)
	h.Handle("/multiKart/login/", loginHandler)
	h.Handle("/multiKart/user/", userHandler)
	h.Handle("/multiKart/catalogue/", catalogueHandler)

	logger.Infof("Resource Setup Done.")

	addr := ":7707"
	s := &http.Server{
		Addr:           addr,
		Handler:        h,
		ReadTimeout:    60 * time.Second,
		WriteTimeout:   60 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	logger.Info(s.ListenAndServe())
}
