package loggerutils

import (
	"go.uber.org/zap"
)

// Define your custom logger type.
var loggerObj *zap.SugaredLogger

func GetLogger() *zap.SugaredLogger {
	if loggerObj == nil {
		logger, _ := zap.NewDevelopment()
		defer logger.Sync() // flushes buffer, if any
		loggerObj = logger.Sugar()
		loggerObj.Infof("Initialized Logger: %s", "Suggared Logger")
	}

	return loggerObj
}
